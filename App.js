import React from 'react';
import { StyleSheet, View } from 'react-native';
import File_Icon from './assets/Images/svg/File_Icon';
import Screen from './src/components/Screen';
import Text from './src/components/Text';
import { DATE_TEXT, ORANGE, WHITE } from './src/constants/color';

const App = () => {
  return (
    <Screen>
      <View style={styles.Panel}>
        <View style={styles.PaneLHeader}>
          <File_Icon
            width="50"
            height="50"
          />
          <View style={styles.PanelTile}>
            <Text>Writing Feedback</Text>
            <Text>Science Report</Text>
          </View>
          <View style={styles.PanelStatus}>
            <Text>Pending</Text>
            <View style={styles.StatusDot} />
          </View>
        </View>
        <Text>ScienceAssignment2.docx</Text>
        <Text color={DATE_TEXT}>10 Apr, 1:16 pm</Text>
      </View>
    </Screen>
  )
}

export default App

const styles = StyleSheet.create({
  Panel: {
    margin: 20,
    padding: 20,
    backgroundColor: WHITE,
    borderRadius: 5
  },
  PaneLHeader: {
    flexDirection: "row",
    marginLeft: -5
  },
  PanelTile: {
    flex: 1,
    paddingLeft: 10
  },
  PanelStatus: {
    flexDirection: 'row'
  },
  StatusDot: {
    width: 10,
    height: 10,
    borderRadius: 5,
    backgroundColor: ORANGE,
    margin: 10
  }

})