import React from "react";
import { SafeAreaView, StyleSheet } from "react-native";

const Screen = ({ children, ...prop }) => {
    return <SafeAreaView style={styles.container}>{children}</SafeAreaView>;
};

export default Screen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#f7f7f7",
        width: "100%",
    },
});
