import React from "react";
import { Text as NativeText } from "react-native";
import { REGULAR_TEXT } from './../constants/color';


const Text = ({ color, fontFamily, lineHeight, fontSize, children, style }) => {
  return (
    <NativeText style={[{
      color,
      fontFamily,
      lineHeight,
      fontSize
    }, style]}
    >
      {children}
    </NativeText >
  );
};


Text.defaultProps = {
  color: REGULAR_TEXT,
  fontFamily: 'Ubuntu-R',
  fontSize: 16,
  lineHeight: 20
};

export default Text;

